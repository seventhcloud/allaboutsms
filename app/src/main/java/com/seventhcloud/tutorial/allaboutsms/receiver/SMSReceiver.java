package com.seventhcloud.tutorial.allaboutsms.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by X-tivity on 6/4/16 AD.
 */
public class SmsReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {

            SmsMessage[] msgs = null;

            if (Build.VERSION.SDK_INT >= 19) {
                msgs = Telephony.Sms.Intents.getMessagesFromIntent(intent);
            } else {
                msgs = getMessagesFromIntent(intent);
            }

            for (SmsMessage sms : msgs) {
                Log.v("handleSmsReceived", (sms.isReplace() ? "(replace)" : ""));
                Log.i("OriginatingAddress", sms.getDisplayOriginatingAddress());
                Log.i("DisplayMessageBody", sms.getDisplayMessageBody());
                Log.i("MessageBody", sms.getMessageBody());
                Log.i("tTimestampMillis", ""+sms.getTimestampMillis());
                Log.i("PseudoSubject", sms.getPseudoSubject());
                Log.i("IndexOnIcc", sms.getIndexOnIcc()+"");
                Log.i("ProtocolIdentifier", sms.getProtocolIdentifier()+"");
                Log.i("getUserData", sms.getUserData().toString());

                String message = sms.getMessageBody();
            }
        }
    }

    @Deprecated
    public SmsMessage[] getMessagesFromIntent(Intent intent) {
        Object[] messages = (Object[]) intent.getSerializableExtra("pdus");

        int pduCount = messages.length;

        SmsMessage[] msgs = new SmsMessage[pduCount];

        for (int i = 0; i < pduCount; i++) {
            byte[] pdu = (byte[]) messages[i];
            msgs[i] = SmsMessage.createFromPdu(pdu);
        }
        return msgs;
    }
}
