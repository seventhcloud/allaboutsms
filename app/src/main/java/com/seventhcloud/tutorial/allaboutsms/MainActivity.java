package com.seventhcloud.tutorial.allaboutsms;

import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    // For Test Send an SMS from window
    // http://www.tutorialspoint.com/android/android_emulator.htm

    // For Test Send an SMS from mac
    //http://stackoverflow.com/a/21819219/2077479
    //
    // in terminal
    // telnet localhost <console-port>
    // nano .emulator_console_auth_token (might need key)
    // auth <token> (from above sep)
    // help (for more info)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSmsInbox();
    }

    private void getSmsInbox() {
        // public static final String INBOX = "content://sms/inbox";
        // public static final String SENT = "content://sms/sent";
        // public static final String DRAFT = "content://sms/draft";
        Cursor cursor = getContentResolver().query(
                Uri.parse("content://sms/inbox"), null, null, null, null);

        if (cursor.moveToFirst()) { // must check the result to prevent exception
            do {
                String msgData = "";
                for(int idx=0;idx<cursor.getColumnCount();idx++)
                {
                    msgData += " " + cursor.getColumnName(idx) + ":" + cursor.getString(idx);
                }
                Log.e("TAG", msgData);
                // use msgData
            } while (cursor.moveToNext());


        } else {
            // empty box, no SMS
        }
    }
}
